<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('/profile','ProfileController')->middleware('auth');


// Route::get('/articles','ArticleController@index');
// Route::get('/articles/create','ArticleController@create');
// Route::post('/articles','ArticleController@store');
// Route::get('/articles/{articles_id}','ArticleController@show');
// Route::get('/articles/{articles_id}/edit','ArticleController@edit');
// Route::put('/articles/{articles_id}','ArticleController@update');
// Route::delete('/articles/{articles_id}','ArticleController@destroy');


Route::get('/test-dompdf', function(){
    $pdf = App::make('dompdf.wrapper');
    $pdf->loadHTML('<h1>Test</h1>');
    return $pdf->stream();
});

// Route articles:
// Route::get('/articles','ArticleController@index');
// Route::get('/articles/create','ArticleController@create');
// Route::post('/articles','ArticleController@store');
// Route::get('/articles/{id}','ArticleController@show');
// Route::get('/articles/{id}/edit','ArticleController@edit');
// Route::put('/articles/{id}','ArticleController@update');
// Route::delete('/articles/{id}','ArticleController@destroy');
Route::resource('articles','ArticleController')->middleware('auth');


// Route comments:
// Route::get('/comments','CommentController@index');
// Route::get('/comments/create','CommentController@create');
// Route::post('/comments','CommentController@store');
// Route::get('/comments/{id}','CommentController@show');
// Route::get('/comments/{id}/edit','CommentController@edit');
// Route::put('/comments/{id}','CommentController@update');
// Route::delete('/comments/{id}','CommentController@destroy');
Route::resource('comments','CommentController')->middleware('auth');

//Route::get('/comments/create/{id}','CommentController@create');
//Route::post('/comments/{id}','CommentController@store');