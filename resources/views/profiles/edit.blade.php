@extends('layouts.master')

@section('content')

  <section class="content mt-3 mr-3 ml-3">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-8 offset-2">
          <!-- general form elements -->
          <div class="card card-outline card-info rounded-0">
            <div class="card-header">
              <h3 class="card-title font-weight-bold">EDIT PROFILE</h3>
            </div>
            @foreach ($profiles as $object)
            <!-- /.card-header -->
            <!-- form start -->
            <form method="post" action="{{route('profile.update',$object->user_id)}}">
              @csrf
              @method('PUT')
              <div class="card-body">
                
                <div class="form-group">
                  <label for="fullname" class="font-weight-bold">Full Name</label>
                  <input type="text" class="form-control rounded-0" id="fullname" name ="fullname" value="{{$object->full_name}}">
                </div>

                @error('fullname')
                  <div class="alert alert-danger">{{$message}}</div>
                @enderror

                <div class="form-group">
                  <label for="username" class="font-weight-bold">Username</label>
                  <input type="text" class="form-control rounded-0" id="username" name ="username" value="{{$object->username}}">
                </div>

                @error('username')
                  <div class="alert alert-danger">{{$message}}</div>
                @enderror

                <div class="form-group">
                  <label for="email" class="font-weight-bold">e-Mail</label>
                  <input type="email" class="form-control rounded-0" id="email" name ="email" value="{{$object->email}}">
                </div>

                @error('email')
                  <div class="alert alert-danger">{{$message}}</div>
                @enderror

                <div class="form-group">
                  <label for="address" class="font-weight-bold">Address</label>
                  <input type="text" class="form-control rounded-0" id="address" name ="address" value="{{$object->address}}">
                </div>

                <div class="form-group">
                  <label for="phone" class="font-weight-bold">Phone Number</label>
                  <input type="text" class="form-control rounded-0" id="phone" name ="phone" value="{{$object->phone}}">
                </div>
                
                @endforeach
                <span><button type="submit" class="btn btn-info btn-flat float-right mb-4"><i class="fas fa-paper-plane"></i> Edit</button></span>
              </div>
              <!-- /.card-body -->
              
              
            </form>
            <span><a href="" class="btn btn-sm btn-danger btn-flat float-left mb-4 ml-3">Delete Account</a><span class="pl-2 text-muted"> Deleting your account will no longer be able to access your articles.</span></span>
          </div>
          <!-- /.card -->  
        </div>
      </div>
    </div>
  </section>

@endsection