@extends('layouts.master')

@section('content')
    <div class="ml-3 mt-3 mr-3">
    <div class="card card-primary">
        <div class="card-header">
        <h3 class="card-title">{{ $article->title }}</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form role="form" href="/comments">
        @csrf
        <div class="card-body">
            <div class="form-group">
                <label for="body"></label>
                {{ $article->body }}
            </div>
            
        </div>
        
        </form>
    </div>
</div>
@endsection