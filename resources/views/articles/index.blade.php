@extends('layouts.master')

@section('content')
<section class="content mt-3 mr-3 ml-3">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                    <h3 class="card-title">Article Table</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        @if(session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif
                        <a class="btn btn-primary mb-3" href="/articles/create">Create New Article</a>
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>Title</th>
                                <th>Body</th>
                                <th style="width: 40px">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($articles as $key => $article)
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td>{{ $article->title }}</td>
                                    <td>{{ $article->body }}</td>
                                    <td style="display: flex;">
                                        <a href="/articles/{{$article->id}}" class="btn btn-info btn-sm">Show</a>
                                        <a href="/articles/{{$article->id}}/edit" class="btn btn-warning btn-sm ml-3 mr-3">Edit</a>
                                        <form action="/articles/{{$article->id}}" method="post">
                                            @csrf
                                            @method('DELETE')
                                            <input type="submit" value="delete" class="btn btn-danger btn-sm">
                                        </form>
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                <td colspan="4" align="center">No Articles</td>
                                </tr>
                            @endforelse
                        
                            </tbody>
                            
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>    
@endsection