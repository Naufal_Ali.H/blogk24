<!-- comments.index -->
@extends('layouts.master')

@section('content')
<div class="mt-3 mr-3 ml-3">
    <div class="card">
        <div class="card-header">
        <h3 class="card-title">Comment Table</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
        @if(session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
        <a class="btn btn-primary mb-3" href="/comments/create">Create New Comment</a>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th style="width: 10px">#</th>
                <th>Body</th>
                <th style="width: 40px">Action</th>
            </tr>
            </thead>
            <tbody>
            @forelse($comments as $key => $comment)
                <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>{{ $comment->body }}</td>
                    <td style="display: flex;">
                        <a href="/comments/{{$comment->id}}" class="btn btn-info btn-sm">Show</a>
                        <a href="/comments/{{$comment->id}}/edit" class="btn btn-warning btn-sm ml-3 mr-3">Edit</a>
                        <form action="/comments/{{$comment->id}}" method="post">
                            @csrf
                            @method('DELETE')
                            <input type="submit" value="delete" class="btn btn-danger btn-sm">
                        </form>
                    </td>
                </tr>
                @empty
                    <tr>
                    <td colspan="4" align="center">No comments</td>
                    </tr>
            @endforelse
        
            </tbody>
        </table>
        </div>

    </div>
</div>
@endsection