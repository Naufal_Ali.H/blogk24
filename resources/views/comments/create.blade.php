<!-- comments.create -->
@extends('layouts.master')

@section('content')
<div class="ml-3 mt-3 mr-3">
    <div class="card card-primary">
        <div class="card-header">
        <h3 class="card-title">Create New Comment</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form role="form" action="/comments" method="POST">
        @csrf
        <div class="card-body">
            <div class="form-group">
                <label for="body"></label>
                <textarea class="form-control" cols=30 rows=10 id="body" name="body" value="{{ old('body','') }}" placeholder="Type Your Comment Here"></textarea>
                @error('body')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            
        </div>
        <!-- /.card-body -->

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Create</button>
            </div>
        </form>
    </div>
</div>
    
@endsection