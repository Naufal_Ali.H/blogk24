<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Blogk24</title>

  <!-- Bootstrap core CSS -->
  <link href="{{asset('/blogpost/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="{{asset('/blogspot/css/blog-post.css')}}" rel="stylesheet">
  <link href="{{ asset('css/styles.css') }}" rel="stylesheet">

</head>

<body>
  @include('sweetalert::alert')
  <!-- Navigation -->
  @include('layouts.partials.navigation')

  <!-- Page Content -->
  <div class="container" style="margin-top:55px">

    <div class="row">

      <!-- Post Content Column -->
      <div class="col-md-12">

        <!-- Post Content -->
        <div class="mt-4 py-4 bodi">
          @yield('content')
        </div>
      
      </div>


    </div>
    <!-- /.row -->

  </div>
  <!-- /.container -->

  <!-- Footer -->
  <footer class="py-5 bg-dark fixed-bottom">
    <div class="container">
      <p class="m-0 text-center text-white">Copyright &copy; Blogk24 2021</p>
    </div>
    <!-- /.container -->
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="{{asset('/blogpost/vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{asset('/blogpost/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

</body>

</html>
