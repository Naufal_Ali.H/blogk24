<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand" href="{{ url('/articles') }}">BLOGK24</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
            <!-- Authentication Links -->
            @guest
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                </li>
                @if (Route::has('register'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                    </li>
                @endif
            @else
                <li class="nav-item dropdown">
                    <a style="color:#000000" id="navbarDropdown" class=" btn btn-sm btn-warning dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{ Auth::user()->username }} <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-right text-right">
                        <li class="dropdown-item">
                            <div class="media">
                                <div class="media-left">
                                <img class="mt-2" src="{{asset('images/profile.png')}}" style="width:auto;height:50px">
                                </div>
                                <div class="media-body">
                                <h6 class="media-heading">{{ Auth::user()->email }}</h6>
                              
                                <h6 class="media-heading">Article: <b> </b></h6>
                               
                                <h6 class="media-heading">Comment:</b></h6>
                                <a href="{{route('profile.edit',Auth::user()->id)}}" class="btn btn-info btn-sm">Edit Profile</a>
                                
                                </div>
                            </div>
                                <!-- <div class="media">
                                    <img src="{{asset('images/profile.png')}}" class="img-size-50 mr-3" style="width:30%">
                                    <div class="media-body">
                                        
                                        {{ Auth::user()->email }}
                                        
                                        
                                        <span><a class="btn btn-info btn-sm">Edit Profile</a></span>
                                    </div>
                                </div> -->
                            
                        </li>
                        <li><hr class="dropdown-divider"></li>
                        <li><a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                          document.getElementById('logout-form').submit();">{{ __('Logout') }}</a></li>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                    </ul>        
                    <!-- <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                          document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div> -->
                </li>
            @endguest
        </ul>
        <!-- <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="#">Home
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">About</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Services</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Contact</a>
          </li>
        </ul> -->
      </div>
    </div>
  </nav>