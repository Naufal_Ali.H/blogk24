<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $table = "articles";

    protected $fillable = ["title","body"];

    public function author(){
        return $this->belongsTo('App\Models\User','user_id');
    }

    public function comment(){
        return $this->hasMany('App\Comment');
    }

}
