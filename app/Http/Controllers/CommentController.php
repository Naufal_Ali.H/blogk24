<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Article;
use App\Models\User;
use App\Comment;
use Auth;
use RealRashid\SweetAlert\Facades\Alert;

class CommentController extends Controller
{
    public function create(){
        return view('comments.create');
    }

    public function store(Request $request){
         //dd($request->all());
        $request->validate([
            //'title' => 'required|unique:comments',
            'body' => 'required'
        ]);

        // $query = DB::table('comments')->insert([
        //     //"title" => $request["title"],
        //     "body" => $request["body"]          
        // ]);
        $user = Auth::user()->id;
        $article = Article::with('comments')->find($id);
        $comment = Comment::create([
            "body" => $request["body"],
            "user_id" => $user,
            "article_id" => $article
        ]);
        Alert::success('Success','Comment has been added!!');
        return redirect('/comments');
    }

    public function index(){
        //$comments = DB::table('comments')->get();
        $comments = Comment::all();
        return view('comments.index', compact('comments'));
    }

    public function show($id){
        //$comment = DB::table('comments')->where('id', $id)->first();
        $comment = Comment::find($id);
        return view('comments.show', compact('comment'));
    }

    public function edit($id){
        $comment = DB::table('comments')->where('id', $id)->first();
        return view('comments.edit', compact('comment'));
    }

    public function update($id, Request $request){
        // $request->validate([
        //     //'title' => 'required|unique:comments',
        //     'body' => 'required'
        // ]);

        // $query = DB::table('comments')
        //             ->where('id', $id)
        //             ->update([
        //                 //'title' => $request['title'],
        //                 'body' => $request['body']
        //             ]);

        $update = Comment::where('id',$id)->update([
            "body" => $request["body"]
        ]);

        return redirect('/comments')->with('success', 'Update successful!');
    }

    public function destroy($id){
        // $query = DB::table('comments')->where('id', $id)->delete();
        Comment::destroy($id);
        return redirect('/comments')->with('success', 'comment has been deleted!');
    }
}
