<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use App\User;
use App\Article;
use DB;
use Auth;
use App\Comment;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        return view('/articles');
    }

    // public function count(){
    //     $user = Auth::user()->id; 
    //     $articles = 0;
    //     $articles = Article::where('user_id', '=',40)->count();
    //     $comments = Comment::where('user_id', '=', $user)->count();    

    //     //dd($count);
    //     return view('layouts.partials.navigation', compact('articles','comments')); 
    // }
    
}
