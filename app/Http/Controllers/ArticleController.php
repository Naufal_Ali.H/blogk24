<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Article;
use App\Models\User;
use Auth;
use RealRashid\SweetAlert\Facades\Alert;

class ArticleController extends Controller
{
    public function create(){
        return view('articles.create');
    }

    public function store(Request $request){
         //dd($request->all());
        $request->validate([
            'title' => 'required|unique:articles',
            'body' => 'required'
        ]);

        // $query = DB::table('articles')->insert([
        //     "title" => $request["title"],
        //     "body" => $request["body"]          
        // ]);

        // $article = new Article;
        // $article->title = $request["title"];
        // $article->body = $request["body"];
        // $article->save();

        $article = Article::create([      
            "title" => $request["title"],
            "body" => $request["body"]          
        ]);

        return redirect('/articles')->with('success', 'article has been saved!');
    }

    public function index(){
        //$articles = DB::table('articles')->get();
        $articles = Article::all();
        return view('articles.index', compact('articles'));
    }

    public function show($id){
        //$article = DB::table('articles')->where('id', $id)->first();
        //$article = Article::find($id);
        $articles = Article::Join('users','users.id','=','articles.user_id')
                    ->where('articles.id', $id)
                    ->get(['articles.*', 'users.username']);
        return view('articles.show', compact('articles'));
    }

    public function edit($id){
        $user = Auth::user()->id; 
        $article = Article::find($id);
        if($user == $article->user_id){
            $article = DB::table('articles')->where('id', $id)->first();
            return view('articles.edit', compact('article'));
        }
        else{
            Alert::error('Oops','Its not your article, cannot update!');
            return redirect('/articles');    
        }
    }

    public function update($id, Request $request){
        // $request->validate([
        //     'title' => 'required|unique:articles',
        //     'body' => 'required'
        // ]);

        // $query = DB::table('articles')
        //             ->where('id', $id)
        //             ->update([
        //                 'title' => $request['title'],
        //                 'body' => $request['body']
        //             ]);

        $update = Article::where('id',$id)->update([
            "title" => $request["title"],
            "body" => $request["body"]
        ]);
        Alert::success('Success','Article has been update!!');
        return redirect('/articles')->with('success', 'Update successful!');
    }

    public function destroy($id){
        //$query = DB::table('articles')->where('id', $id)->delete();
        $user = Auth::user()->id; 
        $article = Article::find($id);
        if($user == $article->user_id){
            $res = Article::destroy($id);
            Alert::success('Success','Article has been deleted!!');
            return redirect('/articles');
        }
        else{
            Alert::error('Oops','Its not your article!');
            return redirect('/articles');     
        }
        // if($res){
        //     return redirect('/articles')->with('success', 'article has been deleted!');
        // }
        // else{
        //     return redirect('/articles')->with('Its not your article!');   
        // }
        
    }

}
