<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Profile extends Model
{
    protected $fillable = [
        'full_name', 'address','phone','user_id'
    ];

    public function user()
    {
        return $this->belongsTo('App/Models/User');
    }
}
